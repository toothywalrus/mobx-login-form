import React, { Component } from 'react';
import LoginFormButton from './LoginFormButton';
import './LoginForm.css';

import { observer } from 'mobx-react';

let inputValue = ''
const LoginForm = observer((LoginFormStore) => (

    <div className="LoginForm_container">
      <h1 className="LoginForm_title">Login</h1>
      <input type="text" className="LoginForm__input" defaultValue="hello" />
      <input type="text" className="LoginForm__input" placeholder="Password" />
      <LoginFormButton store={LoginFormStore} login={inputValue}/>
    </div>
));

export default LoginForm
