import React, { Component } from 'react';

class LoginFormButton extends Component {
  getLogin() {
    console.log(this.props.login)
  }
  render() {
    console.log(this.props.store)
    return (
      <div>
        <button onClick={() => this.getLogin()} className="LoginFormButton">Login</button>
      </div>
    )
  }
}

export default LoginFormButton
