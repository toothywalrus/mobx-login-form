import React, { Component } from 'react';
import LoginForm from '../Components/LoginForm';
import { Provider } from 'mobx-react';
import { useStrict } from 'mobx';
import LoginFormStore from '../Store/LoginFormStore';
import LoginSuccessful from '../Components/LoginSuccessful'

useStrict(true);

const stores = { LoginFormStore }
const auth = false;

const App = ( {props} ) => {
  if(!auth) {
    return (
      <Provider { ...stores }>
        <div>
          <LoginForm />
        </div>
      </Provider>
    )
  }
  else {
    return (
      <Provider { ...stores }>
        <div>
          <LoginSuccessful />
        </div>
      </Provider>
    )
  }
}

export default App;
